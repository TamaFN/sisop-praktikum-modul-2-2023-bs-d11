#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>

// Fungsi untuk membuat directory
void makeDir(char* dirname){
    pid_t child_pid = fork();
    if(child_pid < 0){
        exit(EXIT_FAILURE);
    }
    if(child_pid == 0){
        // Membuat direktori dengan menggunakan fungsi mkdir
        char *argv[] = {"mkdir", dirname, NULL};
        execv("/usr/bin/mkdir", argv);
    }
    while(wait(NULL) != child_pid);
}

// Fungsi untuk menghapus directory
void deleteDir(char *dirname) {
    pid_t child_pid = fork();
    if(child_pid == 0){
        // Menghapus direktori dengan menggunakan fungsi rm
        char *argv[]={"rm", "-r", dirname, NULL};
        execv("/usr/bin/rm", argv);
    }
}

// Fungsi untuk download image pada link https://picsum.photos/
void download_img(char* dirname){
    // Deklarasi
    char filename[20], downlink[20], dirloc[50];
    char *url = {"https://picsum.photos/%d"};
    int status;

    // Download image sebanyak 15 kali
    for(int i=1; i<=15; i++){
        pid_t pid = fork();
        if(pid == 0){
            // Ambil waktu saat ini
            time_t now = time(NULL);
            struct tm *time = localtime(&now);
            // Deklarasi size image
            int size = ((time->tm_sec) % 1000) + 50;

            // membuat format nama file gambar sesuai dengan timestamp
            strftime(filename, 20, "%Y-%m-%d %H:%M:%S.png", time);
            // membuat path direktori dengan format string dirname dan filename ke dalam variabel dirloc
            sprintf(dirloc, "%s/%s", dirname, filename);
            // membuat download link dengan format url(https://picsum.photos/) dan size gambar ke dalam variabel downlink
            sprintf(downlink, url, size);

            // download image menggunakan wget
            char *argv[]={"wget", "-qO", dirloc, downlink, NULL};
            execv("/usr/bin/wget", argv);
        }
        // Image akan di download setiap 5 detik
        sleep(5);
    }
    while (wait(&status) > 0);
}

// Fungsi untuk zip directory
void zipDir(char *dirname){
    char zipname[50];
    sprintf(zipname, "%s.zip", dirname);

    pid_t child_pid = fork();
    if(child_pid == 0){
        char *argv[] = {"zip", "-r", zipname, dirname, NULL};
        execv("/usr/bin/zip", argv);
    }
    while(wait(NULL) != child_pid);
}

void makeKiller(char *argv[], pid_t pid, pid_t sid)
{   
    // Membuat file killer yang bisa ditulis
    FILE *killerFile = fopen("killer.c", "w");

    // Membuat progam untuk killer.c
    char code[1000] = ""
    "#include <stdio.h>\n"
    "#include <stdlib.h>\n"
    "#include <unistd.h>\n"
    "#include <wait.h>\n"
    "int main() {\n"
        "pid_t child_id = fork();\n"
        "if (child_id == 0) {\n"
            "%s"
            "execv(\"%s\", argv);\n"
        "}\n"
        "while(wait(NULL) > 0);\n"
        "char *argv[] = {\"rm\", \"killer\", NULL};\n"
        "execv(\"/usr/bin/rm\", argv);\n"
    "}\n";

    // Membuat command kill untuk mode a
    char commandMode[1000];
    if (strcmp(argv[1], "-a") == 0) {
        // Mode a akan melakukan kill pada semua session dengan mengambil sid
        sprintf(commandMode, "char *argv[] = {\"pkill\", \"-9\", \"-s\", \"%d\", NULL};\n", sid);
        fprintf(killerFile, code, commandMode, "/usr/bin/pkill");
    }

    // Membuat command kill untuk mode b
    if (strcmp(argv[1], "-b") == 0) {
        // Mode b hanya akan melakukan kill pada program utama dengan mengambil pid program utama
        sprintf(commandMode, "char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n", getpid());
        fprintf(killerFile, code, commandMode, "/usr/bin/kill");
    }

    // Tutup file killer.c
    fclose(killerFile);

    // Compile file killer.c
    pid = fork();
    if(pid == 0){    
        char *compile[] = {"gcc", "killer.c", "-o", "killer", NULL};
        execv("/usr/bin/gcc", compile);
    }
    while(wait(NULL) != pid);

    // Menghapus file killer.c setelah dijalankan
    pid = fork();
    if (pid == 0) {
    	char *remove[] = {"rm", "killer.c", NULL};
    	execv("/usr/bin/rm", remove);
    }
    while(wait(NULL) != pid);
}

int main(int argc, char* argv[]){
    // Cek argumen
    if(argc != 2 || (strcmp(argv[1], "-a") != 0 && strcmp(argv[1], "-b") != 0)){
        printf("Error: Argumen salah, pilih mode -a atau mode -b!\n");
        exit(EXIT_FAILURE);
    }

    pid_t pid, sid;
    pid = fork();
    
    if(pid < 0){
        exit(EXIT_FAILURE);
    }
    if(pid > 0){
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if(sid < 0){
        exit(EXIT_FAILURE);
    }
    makeKiller(argv, pid, sid);
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    
    while(1){
        // buat folder
        pid_t child_pid;
        char dirname[50];
        time_t now = time(NULL);
        struct tm *time = localtime(&now);
        strftime(dirname, 50, "%Y-%m-%d_%H:%M:%S", time);
        makeDir(dirname);

        child_pid = fork();
        if(child_pid < 0){
            exit(EXIT_FAILURE);
        } else if(child_pid == 0){
            download_img(dirname);
            zipDir(dirname);
            deleteDir(dirname);
        }
        sleep(30);
    }
}

// source:
// https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/tree/master/Modul2#menjalankan-program-secara-bersamaan
// https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/tree/master/Modul2#wait-x-fork-x-exec
// https://stackoverflow.com/questions/22124413/how-to-use-wget-command-using-exec-command-in-linux