#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>

// Deklarasi status
int status;

// Fungsi untuk menghapus semua file yang bukan MU
void removeNotMU(char path[], char state[]){
	while ((wait(&status)) > 0);
	pid_t child_pid = fork();
	if(child_pid == 0){
	     execlp("find","find", path, "-type", "f", "!", "-name", state, "-delete", NULL);
	     exit(0);
	}
}

// Fungsi untuk membuat grup 
void makeGroup(){
    pid_t child_pid;
    
    // Buat Grup
    child_pid = fork();
    if(child_pid == 0){
    	// Buat directory untuk penyerang
    	execlp("mkdir", "mkdir", "./players/penyerang", NULL);
    	exit(0);
    } else {
    	wait(NULL);
    }
    
    child_pid = fork();
    if(child_pid == 0){
    	// Buat directory untuk bek
    	execlp("mkdir", "mkdir", "./players/bek", NULL);
    	exit(0);
    } else {
    	wait(NULL);
    }
    
    child_pid = fork();
    if(child_pid == 0){
    	// Buat directory untuk gelandang
    	execlp("mkdir", "mkdir", "./players/gelandang", NULL);
    	exit(0);
    } else {
    	wait(NULL);
    }
    
    child_pid = fork();
    if(child_pid == 0){
    	// Buat directory untuk kiper
    	execlp("mkdir", "mkdir", "./players/kiper", NULL);
    	exit(0);
    } else {
    	wait(NULL);
    }
}

// Fungsi buatTim 
void buatTim(int bek, int gelandang, int penyerang){
	pid_t child_pid;
	
	// Kiper
	child_pid = fork();
	if(child_pid == 0){
	     char command[100];
	     snprintf(command, sizeof(command), "ls ./players/kiper | sort -t _ -nk4 | tail -n 1 >> ~/Formation_%d_%d_%d.txt", bek, gelandang, penyerang);
	     execlp("sh", "sh", "-c", command, NULL);
	}
	
	// Bek
	while ((wait(&status)) > 0);
	child_pid = fork();
	if(child_pid == 0){
	     char command[100];
	     snprintf(command, sizeof(command), "ls ./players/bek | sort -t _ -nk4 | tail -n 1 >> ~/Formation_%d_%d_%d.txt",bek, bek, gelandang, penyerang);
	     execlp("sh", "sh", "-c", command, NULL);
	}
	
	// Gelandang
	while ((wait(&status)) > 0);
	child_pid = fork();
	if(child_pid == 0){
	     char command[100];
	     snprintf(command, sizeof(command), "ls ./players/gelandang | sort -t _ -nk4 | tail -n 1 >> ~/Formation_%d_%d_%d.txt", gelandang, bek, gelandang, penyerang);
	     execlp("sh", "sh", "-c", command, NULL);
	}
	
	// Penyerang
	while ((wait(&status)) > 0);
	child_pid = fork();
	if(child_pid == 0){
	     char command[100];
	     snprintf(command, sizeof(command), "ls ./players/penyerang | sort -t _ -nk4 | tail -n 1 >> ~/Formation_%d_%d_%d.txt", penyerang, bek, gelandang, penyerang);
	     execlp("sh", "sh", "-c", command, NULL);
	}
	
}


int main() {
    DIR *dir;
    struct dirent *entry;
    pid_t pid;    
	
    // Download file database dari URL menggunakan wget
    pid = fork();
    if (pid == 0) {
        execl("/usr/bin/wget", "wget", "—no-check-certificate", "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", "-O", "players.zip", NULL);
        exit(0);
    } else {
        wait(NULL);
    }

    // Ekstrak file players.zip menggunakan unzip
    pid = fork();
    if (pid == 0) {
        execl("/usr/bin/unzip", "unzip", "-q", "players.zip", "-d", ".", NULL);
        exit(0);
    } else {
        wait(NULL);
    }

    // Hapus file players.zip dan database.zip menggunakan rm
    pid = fork();
    if (pid == 0) {
        execl("/usr/bin/rm", "rm", "-f", "players.zip", NULL);
        exit(0);
    } else {
        wait(NULL);
    }
    
    // Panggil fungsi makeGroup    
    makeGroup();
    
    // Hapus semua pemain yang bukan MU
    removeNotMU("./players", "*ManUtd*");
    
    // Melakukan Group berdasarkan posisi
    makeGroup();
    
    // Group berdasarkan penyerang
    while ((wait(&status)) > 0);
    pid = fork();
    if(pid == 0){
    	execlp("find", "find", "./players", "-iname", "*Penyerang*", "-exec", "mv", "-n", "{}", "./players/penyerang/", ";", NULL);
    }
    
    // Group berdasarkan bek
    pid = fork();
    if(pid == 0){
    	execlp("find", "find", "./players", "-iname", "*Bek*", "-exec", "mv", "-n", "{}", "./players/bek/", ";", NULL);
    }
    
    // Group berdasarkan gelandang
    pid = fork();
    if(pid == 0){
    	execlp("find", "find", "./players", "-iname", "*Gelandang*", "-exec", "mv", "-n", "{}", "./players/gelandang/", ";", NULL);
    }
    
    // Group berdasarkan kiper
    pid = fork();
    if(pid == 0){
    	execlp("find", "find", "./players", "-iname", "*Kiper*", "-exec", "mv", "-n", "{}", "./players/kiper/", ";", NULL);
    }
    
    
    // Panggil fungsi buatTim dengan formasi 3,4,3
    buatTim(3,4,3);
    
    return 0;
}
